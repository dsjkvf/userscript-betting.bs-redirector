BetStudy Redirector
===================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a match page on [BetStudy](https://www.betstudy.com/today-matches/) -- will instantly redirect to the prediction page for that particular match.
