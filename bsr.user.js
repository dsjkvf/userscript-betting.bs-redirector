// ==UserScript==
// @name        betting8: BS prediction redirector
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-betting8-bs-redirector/
// @downloadURL https://bitbucket.org/dsjkvf/userscript-betting8-bs-redirector/raw/master/bsr.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-betting8-bs-redirector/raw/master/bsr.user.js
// @match       *://*.betstudy.com/*
// @run-at      document-start
// @version     1.0.1
// @grant       none
// ==/UserScript==


// INIT

var url = window.location.href;


// HELPERS

function redirectToPrediction(url, delta = 0) {
    return url.replace(/(.*)soccer-stats\/match(.*)-(\d*)/, "$1prediction\/$3$2")
}

// MAIN

// Immediately redirect to today if an url doesn't contain a link to a date
if ( /-\d\d\d\d\d\d*\/$/.test(url)) {
    window.location.replace(redirectToPrediction(url));
}
